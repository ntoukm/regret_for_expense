<?php
require_once "ChromePhp.php";

$period = filter_input(INPUT_POST, 'period');
$mysqli = new mysqli("localhost", "root", "root","dev_no_kakin");
ChromePhp::log($mysqli);
if ($mysqli->connect_error) {
    echo "データベースに接続できないみたいです：".$mysqli->connect_error;
    exit();
} else {
    $mysqli->set_charset("utf8");
    ChromePhp::log("DBに接続した");
}

switch ($period) {
  case "year":
    // $select_pays = "SELECT * FROM `pays` WHERE `date` > DATE_SUB(CURRENT_DATE(), INTERVAL 1 YEAR) ORDER BY `date` DESC";
    $select_pays = 'SELECT * FROM `pays` WHERE `detail` = "FGO" ORDER BY `date` DESC';
    break;
  case "month":
    // $select_pays = "SELECT * FROM `pays` WHERE `date` > DATE_SUB(CURRENT_DATE(), INTERVAL 1 MONTH) ORDER BY `date` DESC";
    $select_pays = 'SELECT * FROM `pays` WHERE `detail` = "あんスタ" ORDER BY `date` DESC';
    break;
  case "week":
    $select_pays = "SELECT * FROM `pays` WHERE `date` > DATE_SUB(CURRENT_DATE(), INTERVAL 1 WEEK) ORDER BY `date` DESC";
    // $select_pays = 'SELECT * FROM `pays` WHERE `detail` = "FGO" ORDER BY `date` DESC';
    break;
}

$results = mysqli_query($mysqli, $select_pays) ? : "エラー:".die($mysqli->error);

// JSONに整形
while ($row = $results->fetch_assoc()) {
  $pays[] = [
    "date"    => $row['date'],
    "amount"  => $row['amount'],
    "detail"  => $row['detail'],
    "purpose" => $row['purpose'],
  ];
}

header('Content-Type: application/json');
echo json_encode($pays);
// return json_encode($pays);
// return $pays;
