<?php

require_once dirname("index.php").'/library.php';

// headerレイアウトの読み込み
readfile(dirname('index.php').'/layouts/header.html');
?>

<body>

  <p class="flash">登録が完了しました！</p>

  <a href="top.php">トップへ</a>

<?php
// ログインモーダルの読み込み
readfile(dirname('index.php').'/layouts/login_modal.html');
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
<script src="kakin.js"></script>
<script>
  $(function() {
    $("#datepicker").datepicker();
  });
</script>

</body>
</html>
