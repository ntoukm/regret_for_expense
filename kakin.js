// 金額のカウントアップ
$(function() {
  price = $(".count-up").attr("data-num");
  $({count: 0}).animate({count: price}, {
    easing: "swing",
    //間隔が短いと大きな金額をカウントアップする際に処理が追いつかない？
    duration: 2000,
    step: function() {
      $(".count-up").text(Math.ceil(this.count));
    }
  });
});

// 表示するデータの期間変更
$(function() {
  $("select[name=data-period]").change(function() {
    $.ajax({
      url: "api.php",
      type: "POST",
      dataType: "json",
      data: {
        period: $(this).val(), // $("select[name=data-period]").val()
      },
    }).done(function(response) {
      // var parseResponse = $.parseJSON(response);
      $(".kakin-list-row").remove();
      if (!response) {
        tableRow = '<tr class="kakin-list-row">'
                    + '<td class="col-12 text-center">この期間、無課金です！</td>';

        $(tableRow).appendTo("#kakin-list").hide().fadeIn(400);

        // $("#kakin-list").append(
        //   $('<tr class="kakin-list-row">').append(
        //     $('<td class="col-12 text-center">').text("この期間、無課金です！")
        //   )
        // );
      } else {
        // apiの返り値を描画
        for (i = 0; i < response.length; i++) {
          commaSeparatedAmount = String(response[i]['amount']).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,'); // 金額をカンマ区切りにする
          tableRow = '<tr class="kakin-list-row">'
                      + '<td class="col-2 text-right small-letter">' + response[i]['date'] +'</td>'
                      + '<td class="col-2 text-right">' + commaSeparatedAmount + '</td>'
                      + '<td class="col-3 text-center">' + response[i]['detail'] + '</td>'
                      + '<td class="col-5">' + response[i]['purpose'] + '</td></tr>';

          $(tableRow).appendTo("#kakin-list").hide().fadeIn(400);
          // $("#kakin-list").append(
          //   $('<tr class="kakin-list-row">').append(
          //     $('<td class="col-2 text-right small-letter">').text(response[i]['date'])
          //   ).append(
          //     $('<td class="col-2 text-right">').text(response[i]['amount'])
          //   ).append(
          //     $('<td class="col-3 text-center">').text(response[i]['detail'])
          //   ).append(
          //     $('<td class="col-5">').text(response[i]['purpose'])
          //   )
          // );
        }
      }
    }).fail(function(XMLHttpRequest, textStatus, errorThrown) {
      window.alert("【" + XMLHttpRequest.status + " " + textStatus + "】" + errorThrown.message);
    });
  });
});


// モーダル
$(function() {
  $("#modal-login-open").click(function() {
    modal = "#modal-login";
    modalAction(modal);
  });
  // $("#modal-pays-open").click(function() {
  //   modal = "#modal-wants";
  //   removeModal(modal);
  //   modalAction("#modal-pays");
  // });
  // $("#modal-wants-open").click(function() {
  //   modal = "#modal-pays";
  //   removeModal(modal);
  //   modalAction("#modal-wants");
  // });

  // モーダル開閉
  function modalAction(modal) {
    $("body").append('<div id="modal-overlay"></div>');
    $("#modal-overlay").fadeIn("slow");
    centeringModal(modal);
    $(modal).fadeIn("slow");

    $("#modal-close, #modal-overlay").unbind().click(function() {
      $(modal).fadeOut("slow");
      $("#modal-overlay").fadeOut("slow", function() {
        $("#modal-overlay").remove();
      });
    });
  }

  // 既に他のモーダルが開かれている場合はそれを閉じる
  function removeModal(modal) {
    if ($(modal).size()) {
      $(modal).fadeOut("slow");
    }
  }

  // モーダルを画面中央に配置
  function centeringModal(modal) {
    width       = $(window).width();
    height      = $(window).height();
    modalWidth  = $(modal).outerWidth();
    modalHeight = $(modal).outerHeight();
    pxLeft      = ((width - modalWidth) / 2);
    pxTop       = ((height - modalHeight) / 2) + 70;
    $(modal).css({
      "left": pxLeft + "px",
      "top": pxTop + "px"
    });
  }
});
