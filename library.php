<?php

function h($str) {
    return htmlspecialchars($str, ENT_QUOTES, 'UTF-8');
}


// [TODO] 以下はPDO、modelファイルを実装したら削除する
// 課金履歴を取得（年間）
function getPaysPerYear($mysqli, $user_id) {
    $sql = "SELECT * FROM `pays`
              WHERE `date` > DATE_SUB(CURRENT_DATE(), INTERVAL 1 YEAR)
              AND `user_id` = ".mysqli_real_escape_string($mysqli, $user_id)."
              ORDER BY `date` DESC;";

    $result = mysqli_query($mysqli, $sql);
    return $result;
}

// 課金履歴を取得（全て）
function getPaysAll($mysqli, $user_id) {
    $sql = "SELECT * FROM `pays`
              WHERE `user_id` = ".mysqli_real_escape_string($mysqli, $user_id)."
              ORDER BY `date` DESC;";

    $result = mysqli_query($mysqli, $sql);
    return $result;
}

// 課金履歴を取得（直近の一回）
function getPaysLatest($mysqli, $user_id) {
    $sql = "SELECT * FROM `pays`
              WHERE `user_id` = ".mysqli_real_escape_string($mysqli, $user_id)."
              ORDER BY `date` DESC LIMIT 1;";

    $result = mysqli_query($mysqli, $sql);
    return $result;
}

// 課金履歴登録
function insertPay($mysqli, $user_id, $date, $amount, $detail, $purpose) {
    $sql = "INSERT INTO `pays` (`user_id`, `date`, `amount`, `detail`, `purpose`, `created_at`)
              VALUES ('".mysqli_real_escape_string($mysqli, $user_id)."', '"
                        .mysqli_real_escape_string($mysqli, $date)."', '"
                        .mysqli_real_escape_string($mysqli, $amount)."', '"
                        .mysqli_real_escape_string($mysqli, $detail)."', '"
                        .mysqli_real_escape_string($mysqli, $purpose)."', '"
                        .date('Y-m-d H:i:s')."');";

    $result = mysqli_query($mysqli, $sql);
    return $result;
}

// ほしいもの登録
function insertWant($mysqli, $user_id, $name, $amount) {
    $sql = "INSERT INTO `wants` (`name`, `amount`, `created_at`)
              VALUES ('".mysqli_real_escape_string($mysqli, $name)."', '"
                        .mysqli_real_escape_string($mysqli, $amount)."', '"
                        .date('Y-m-d H:i:s')."');";

    $result = mysqli_query($mysqli, $sql);
    return $result;
}

// ユーザー登録
function insertUser($mysqli, $name, $email, $password_hashed, $twitter_id) {
    $sql = "INSERT INTO `users` (`name`, `email`, `password`, `twitter_id`)
              VALUES ('".$name."', '".$email."', '".$password_hashed."', '".$twitter_id."')";

    $result = mysqli_query($mysqli, $sql);
    return $result;
}

// メールアドレスが既に登録されているユーザーと重複しないか
function checkEmailExistence($mysqli, $email) {
    $sql = "SELECT 1 FROM `users`
              WHERE `email` = '".$email."'";

    $result = mysqli_query($mysqli, $sql);
    return $result->num_rows;
}

// 登録済みユーザーであればユーザーIDを返す
function checkUserExistenceFromTwitterOAuth($mysqli, $user_id) {
    $sql = "SELECT `id` FROM `users`
              WHERE `twitter_id` = $user_id";

    $result = mysqli_query($mysqli, $sql);
    $result = $result->num_rows ? ($result->fetch_row())[0] : "";
    return $result;
}
