<?php

define('DB_HOST', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'root');
define('DB_NAME', 'dev_no_kakin');

function new_mysqli() {
  $mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

  if ($mysqli->connect_error) {
      die("データベースに接続できないみたいです：".$mysqli->connect_error);
      exit();
  } else {
      $mysqli->set_charset("utf8");
  }
  return $mysqli;
}
