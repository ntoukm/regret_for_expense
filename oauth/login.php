<?php

session_start();

require_once 'common.php';
require_once 'twitteroauth/autoload.php';

use Abraham\TwitterOAuth\TwitterOAuth;

// TwitterOAuthのインスタンスを生成
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);

// コールバックURLのセット
$request_token = $connection->oauth('oauth/request_token', ['oauth_callback' => OAUTH_CALLBACK]);

// callback.phpで利用
$_SESSION['oauth_token'] = $request_token['oauth_token'];
$_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];

// Twitter.com上の認証画面のURLを取得しリダイレクト
$url = $connection->url('oauth/authenticate', ['oauth_token' => $request_token['oauth_token']]);
header('location:'.$url);
