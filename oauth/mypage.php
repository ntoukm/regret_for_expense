<?php

session_start();

require_once 'common.php';
require_once 'twitteroauth/autoload.php';
require_once '../library.php';
require_once '../mysqli.php';

use Abraham\TwitterOAuth\TwitterOAuth;

// callback.phpでセッションに入れた配列
$access_token = $_SESSION['access_token'];

// OAuthトークンを利用してTwitterOAuthをインスタンス化
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);

// ユーザー情報を取得
$twitteroauth_user = $connection->get('account/verify_credentials');

// 初回ログインかどうか
$mysqli = new_mysqli();
$user_id = checkUserExistenceFromTwitterOAuth($mysqli, $twitteroauth_user->id);

if ($user_id) {
  $_SESSION['user_id'] = $user_id;
  header('location: /regret_for_expense/top.php');
} else {
  // ユーザー登録画面にTwitterのユーザー情報を渡す
  $_SESSION['twitter_user_name'] = $twitteroauth_user->name;
  $_SESSION['twitter_user_id'] = $twitteroauth_user->id;
  header('location: /regret_for_expense/signup.php');
}

// echo '<pre>';
// var_dump($twitteroauth_user);
// echo '</pre>';
