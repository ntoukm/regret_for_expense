<?php

session_start();

require_once dirname('index.php').'/library.php';
require_once dirname('index.php').'/mysqli.php';

$errors = [];
$name = isset($_SESSION['twitter_user_name']) ? $_SESSION['twitter_user_name'] : "";
$email = isset($_SESSION['twitter_email']) ? $_SESSION['twitter_email'] : "";
$twitter_id = isset($_SESSION['twitter_user_id']) ? $_SESSION['twitter_user_id'] : "";

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $mysqli = new_mysqli();

  // (1) 項目数分をベタ書きでバリデーション
  // $name             = isset($_POST['name']) ? $_POST['name'] : null;
  // $email            = isset($_POST['email']) ? $_POST['email'] : null;
  // $password         = isset($_POST['password']) ? $_POST['password'] : null;
  // $password_confirm = isset($_POST['password_confirm']) ? $_POST['password_confirm'] : null;

  // (2) 可変変数を使い繰り返し処理とすることで、項目数に関わらず短く書ける
  // foreach (['name', 'email', 'password', 'password_confirm'] as $item) {
  //   $$item = isset($_POST[$item]) ? $_POST[$item] : null;
  // }

  // (3) 可変変数を使用かつfilter_input関数を使用することで、さらに簡潔に書ける
  foreach (['name', 'email', 'password', 'password_confirm'] as $item) {
    $$item = filter_input(INPUT_POST, $item);
  }

  // バリデーション
  if (!$name) {
    $errors[] = "ユーザー名を入力してください。";
  }

  if (!$email) {
    $errors[] = "メールアドレスを入力してください。";
  } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $errors[] = "メールアドレスを正しく入力してください。";
  } elseif (checkEmailExistence($mysqli, $email)) {
    $errors[] = "このメールアドレスはすでに登録されています。";
  }

  if (!$password) {
    $errors[] = "パスワードを入力してください。";
  } elseif (!$password_confirm) {
    $errors[] = "パスワード（確認）を入力してください。";
  } elseif ($password !== $password_confirm) {
    $errors[] = "パスワードが一致しません。再度入力してください。";
  }

  // バリデーションエラーがなければユーザー登録
  if (!$errors) {
    $password_hashed = password_hash($password, PASSWORD_DEFAULT);
    $result = insertUser($mysqli, $name, $email, $password_hashed, $twitter_id);

    if ($result) {
      header('location: complete.php');
    } else {
      $errors[] = "エラーが発生しました。もう一度やり直してください。";
    }
  }
$mysqli->close();
}

// headerレイアウトの読み込み
readfile(dirname('index.php').'/layouts/header.html');
?>

<body>

  <div class="block-row">
    <div class="block-row-left">
      <form action="signup.php" method="post">
        <ul>
          <?php foreach ($errors as $error) : ?>
            <li class="small-letter" style="color: red;"><?= $error ?></li><br />
          <?php endforeach; ?>
          <p class="small-letter">下記の項目を入力し、ユーザー登録してください。</p>
          <li class="li-label">ユーザー名</li>
          <li>
            <input type="text" name="name" value="<?= h($name) ?>" />
          </li><br />
          <li class="li-label">メールアドレス</li>
          <li>
            <input type="text" name="email" value="<?= h($email) ?>" />
          </li><br />
          <li class="li-label">パスワード</li>
          <li>
            <input type="password" name="password" />
          </li><br />
          <li class="li-label">パスワード（確認）</li>
          <li>
            <input type="password" name="password_confirm" />
          </li><br />
          <li>
            <input type="hidden" name="twitter_id" value="<?= h($twitter_id) ?>" />
          </li><br />

          <input type="submit" name="submit" class="button" value="登録" />
        </ul>
      </form>
    </div>
    <?php if (empty($twitter_id)) :?>
    <div class="partation"></div>
    <div class="block-row-right">
      <ul>
        <p class="small-letter">SNSアカウントでのユーザー登録はこちらから。</p>
        <li>
          <a href="./oauth/login.php" class="twitter-login">
            <i class="fa fa-twitter fa-1x"></i> Twitterで利用登録
          </a>
        </li>
      </ul>
    </div>
  <?php endif; ?>
  </div>

<?php
// ログインモーダルの読み込み
readfile(dirname('index.php').'/layouts/login_modal.html');
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
<script src="kakin.js"></script>
<script>
  $(function() {
    $("#datepicker").datepicker();
  });
</script>

</body>
</html>
