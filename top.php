<?php

session_start();

require_once dirname('index.php').'/library.php';
require_once dirname('index.php').'/mysqli.php';

// 変数と定数の初期設定
$mysqli = new_mysqli();
$errors = []; // array()と同義
$message = "";
$user_id = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : "";
// $user_id = 1;

if (isset($_SESSION['user_id'])) {
  $user_id = $_SESSION['user_id'];
} else {
  die("何かエラーが発生したみたいです。申し訳ないですがもう一度ログインし直してください。");
}

const PAYS  = "1";
const WANTS = "2";

// データの保存処理
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['type'])) {
        switch ($_POST['type']) {
            // 課金したものを登録
            case PAYS:
                $date    = $_POST['date'];
                $amount  = $_POST['amount'];
                $detail  = $_POST['detail'];
                $purpose = $_POST['purpose'];
                insertPay($mysqli, $user_id, $date, $amount, $detail, $purpose);
                break;
            // 欲しいものを登録
            case WANTS:
                $name   = $_POST['name'];
                $amount = $_POST['amount'];
                insertWant($mysqli, $user_id, $name, $amount);
                break;
        }
        $message = "登録しました！";
    } else {
        $errors['type'] = '不正な操作みたいです。申し訳ないですがもう一度やり直してください。';
        $message = "登録に失敗しました…";
    }
}

// 各種課金データの取得
$all_pays = getPaysAll($mysqli, $user_id);
$latest_pay = getPaysLatest($mysqli, $user_id);

$mysqli->close();

// headerレイアウトの読み込み
readfile(dirname('index.php').'/layouts/header.html');
?>

<body>

  <div class="text-center">
    <p class="flash"><?= $message ?></p>
    <?php if ($row = $latest_pay->fetch_assoc()) : ?>
      <span class="marker">最後に課金したのは<?= date('n月j日', strtotime($row['date'])) ?>
        、<span class="count-up" data-num="<?= $row['amount'] ?>"></span>円です。</span>
    <?php endif; ?>
  </div>

  <div class="block-row">
    <div class="block-row-left">
      <span class="small-letter">表示データの期間を変更：</span>
      <form name="data-period" style="display: inline-block;">
        <select name="data-period">
          <option value="year" selected>年単位</option>
          <option value="month">月単位</option>
          <option value="week">週単位</option>
        </select>
      </form>

      <table class="list" id="kakin-list">
        <thead class="list-head">
          <tr>
            <th class="col-2"><i class="fa fa-calendar fa-2x"></th>
            <th class="col-2"><i class="fa fa-jpy fa-2x"></i></th>
            <th class="col-3"><i class="fa fa-tag fa-2x"></i></th>
            <th class="col-5"><i class="fa fa-comment fa-2x"></i></th>
          </tr>
        </thead>
        <tbody class="list-body">
          <?php while ($row = $all_pays->fetch_assoc()) : ?>
          <tr class="kakin-list-row">
            <td class="col-2 text-right small-letter"><?= $row['date'] ?></td>
            <td class="col-2 text-right">￥<?= number_format($row['amount']) ?></td>
            <td class="col-3 text-center"><?= $row['detail'] ?></td>
            <td class="col-5"><?= $row['purpose'] ?></td>
          </tr>
          <?php endwhile; ?>
        </tbody>
      </table>
    </div>

    <div class="block-row-right kakin-save">
      <form action="top.php" method="post">
        <!-- <ul> -->
          <li class="li-label small-letter">課金日</li>
          <li><input type="text" name="date" class="number" id="datepicker" /></li><br />
          <li class="li-label small-letter">金額</li>
          <li><input type="text" name="amount" class="number" />　円</li><br />
          <li class="li-label small-letter">ジャンル</li>
          <li><input type="text" name="detail" class="text" /></li><br />
          <li class="li-label small-letter">目的</li>
          <li><input type="text" name="purpose" class="text" /></li><br />
        <!-- </ul> -->

          <input type="hidden" name="type" value="1" />
          <input type="submit" name="submit" class="button" id="modal-close" value="登　録" />
      </form>
    </div>
  </div>

<?php
// ログインモーダルの読み込み
readfile(dirname('index.php').'/layouts/login_modal.html');
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
<script src="kakin.js"></script>
<script>
  $(function() {
    $("#datepicker").datepicker();
  });
</script>

</body>
</html>
